"""
Instead of parsing each HTML file from www.reddit.com, use the API reddit provides
for developers to interact with their data.
http://www.reddit.com/dev/api

we can use urllib.request - "Dive Into Python" recommends other
libraries (like httplib2) but I don't know if you have that.
see http://getpython3.com/diveintopython3/http-web-services.html

There's also a library called requests which makes all of this simpler
    http://docs.python-requests.org/en/latest/
I'm going to use that. You can install it from pip:
    http://pypi.python.org/pypi/pip
Make sure you get the python 3 version of pip so you can use the
python 3 version of requests
"""
import requests
import sys

reddit_url=("http://www.reddit.com/top/.json?sort=top&t=all&"
            "limit=%s&after=%s")

def print_top_posts(num_posts=25):
    after=''
    top_scores=[]

    while (num_posts > 0):
        # reddit limits you to 100 listings per request so only ask for 100 at most
        posts_to_read = 100 if num_posts >= 100 else num_posts
        num_posts -= posts_to_read

        # make the request with the correct parameters
        request_url = reddit_url % (str(posts_to_read),after)
        r = requests.get(request_url)

        # r.json() is a dictionary with all the data from the request
        # r.json()['data']['children'] is a list with all the data about each listing
        stories = r.json()['data']['children']
        # putting after in the url will tell reddit where to start the next request
        after = r.json()['data']['after']

        # for each element in stories, get the score
        for s in stories:
            top_scores.append(s['data']['score'])
            # find other values (like ups, downs, title, etc.) at:
            # http://api.reddit.com/top/?sort=top&t=all

    for v in top_scores:
        print(v)
    print("Number of posts: %d"%(len(top_scores)))

if __name__ == '__main__':
    # Usage: python reddit_top_posts.py <optional number of posts>
    num_posts=25
    if len(sys.argv)==2:
        num_posts=int(sys.argv[1])
    print_top_posts(num_posts)
